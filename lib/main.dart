import 'package:flutter/material.dart';

void main() {
  runApp(HelloFlutterApp());
}

class HelloFlutterApp extends StatefulWidget {
  @override
  State<HelloFlutterApp> createState() => _HelloFlutterAppState();
}

String engGreeting = "Hello Flutter";
String espGreeting = "Hola Flutter";
String thGreeting = "สวัสดี Flutter";
String jpGreeting = "こんにちは Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = engGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.home),
          title: Text("Hello Flutter"),
          actions: [
            IconButton(
              onPressed: () {
                setState(() {
                  displayText =
                      displayText == engGreeting ? espGreeting : engGreeting;
                });
              },
              icon: Icon(Icons.safety_check_sharp),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText =
                      displayText == engGreeting ? thGreeting : engGreeting;
                });
              },
              icon: Icon(Icons.account_balance_rounded),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText =
                      displayText == engGreeting ? jpGreeting : engGreeting;
                });
              },
              icon: Icon(Icons.add_location_alt_rounded),
            ),
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(color: Colors.purpleAccent, fontSize: 24),
          ),
        ),
      ),
    );
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   const HelloFlutterApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           leading: Icon(Icons.home),
//           title: Text("Hello Flutter"),
//           actions: [
//             IconButton(
//               onPressed: () {},
//               icon: Icon(Icons.refresh),
//             )
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(color: Colors.purpleAccent, fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }
